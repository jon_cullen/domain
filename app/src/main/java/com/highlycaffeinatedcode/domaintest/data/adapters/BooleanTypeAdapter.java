package com.highlycaffeinatedcode.domaintest.data.adapters;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;

/**
 * Created by Jon Cullen on 13/01/2018.
 */

public class BooleanTypeAdapter extends TypeAdapter<Boolean> {

    @Override
    public void write(JsonWriter out, Boolean value) throws IOException {
        if (value == null)
            out.nullValue();
        else
            out.value(value);
    }

    @Override
    public Boolean read(JsonReader in) throws IOException {

        JsonToken type = in.peek();
        switch (type) {
            case BOOLEAN:
                return in.nextBoolean();
            case NUMBER:
                return in.nextInt() != 0;
            default:
                throw new IllegalStateException("Expected boolean or number but was " + type);
        }
    }
}
