package com.highlycaffeinatedcode.domaintest.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jon Cullen on 07/01/2018.
 */

public class ListingResults {

    @SerializedName("Listings")
    @Expose
    private List<Listing> listings = new ArrayList<>();

    public List<Listing> getListings() {
        return listings;
    }

    public void setListings(List<Listing> listings) {
        this.listings = listings;
    }
}
