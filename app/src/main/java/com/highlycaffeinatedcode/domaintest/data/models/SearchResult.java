package com.highlycaffeinatedcode.domaintest.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Jon Cullen on 07/01/2018.
 */

public class SearchResult {

    @SerializedName("ListingResults")
    @Expose
    private ListingResults listingResults;

    public ListingResults getListingResults() {
        return listingResults;
    }

    public void setListingResults(ListingResults listingResults) {
        this.listingResults = listingResults;
    }
}
