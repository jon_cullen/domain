package com.highlycaffeinatedcode.domaintest.data.repositories;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.highlycaffeinatedcode.domaintest.app.App;
import com.highlycaffeinatedcode.domaintest.data.SearchApiService;
import com.highlycaffeinatedcode.domaintest.data.models.Listing;
import com.highlycaffeinatedcode.domaintest.data.models.SearchResult;
import com.highlycaffeinatedcode.domaintest.ui.models.PropertyViewModel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import rx.Observable;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.functions.Func1;

/**
 * Created by Jon Cullen on 07/01/2018.
 */

public class ListingRepositoryImpl implements ListingRepository {

    private SearchApiService searchApiService;
    private List<Listing> listings;
    private Set<String> favourites;
    private SharedPreferences sharedPrefs;


    public ListingRepositoryImpl(SearchApiService searchApiService) {
        this.searchApiService = searchApiService;
        this.listings = new ArrayList<>();
        this.favourites = new HashSet<>();

        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        getFavourites();

    }


    @Override
    public Observable<Listing> getPropertyListings(boolean refresh) {
        if (refresh) {
            listings.clear();
        }
        return getPropertyListingsFromMemory().switchIfEmpty(getPropertyListingsFromNetwork());
    }

    @Override
    public Observable<Listing> getPropertyListingsFromMemory() {
        if (this.listings.isEmpty()) {
            return Observable.from(getListings());
        } else {
            return Observable.from(this.listings);
        }
    }

    @Override
    public Observable<Listing> getPropertyListingsFromNetwork() {

        Observable<SearchResult> searchObservable = searchApiService.getMapSearchResults(
                SearchApiService.QueryParamDefault.MODE.toString(),
                SearchApiService.QueryParamDefault.SUBURB.toString(),
                SearchApiService.QueryParamDefault.POSTCODE.toString(),
                SearchApiService.QueryParamDefault.STATE.toString()
        );

        return searchObservable.concatMap(new Func1<SearchResult, Observable<Listing>>() {
            @Override
            public Observable<Listing> call(SearchResult searchResult) {
                return Observable.from(searchResult.getListingResults().getListings());
            }
        }).doOnNext(new Action1<Listing>() {
            @Override
            public void call(Listing listing) {
                listings.add(listing);
            }
        }).doOnCompleted(new Action0() {
            @Override
            public void call() {
                saveListings();
            }
        });
    }

    @Override
    public boolean isFavourite(long adId) {
        return favourites.contains(Long.toString(adId));
    }

    @Override
    public void addFavourite(long adId) {
        favourites.add(Long.toString(adId));
        saveFavourites();
    }

    @Override
    public void removeFavourite(long adId) {
        favourites.remove(Long.toString(adId));
        saveFavourites();
    }

    private static String KEY_FAVS = "favourites";
    private static String KEY_LISTINGS = "listings";

    private void getFavourites() {
        String csv = sharedPrefs.getString(KEY_FAVS, null);
        if (csv != null) {
            this.favourites = new HashSet<>(Arrays.asList(TextUtils.split(csv, ",")));
        }
    }

    private void saveFavourites() {
        TextUtils.join(",", favourites);
        sharedPrefs.edit().putString(KEY_FAVS, TextUtils.join(",", favourites)).apply();
    }

    private List<Listing> getListings() {

        String gson = sharedPrefs.getString(KEY_LISTINGS, null);
        if (gson != null) {
            return Arrays.asList((Listing[]) new Gson().fromJson(gson, Listing[].class));
        }
        return new ArrayList<>();

    }

    private void saveListings() {

        String gson = new Gson().toJson(listings);
        sharedPrefs.edit().putString(KEY_LISTINGS, gson.toString()).apply();


    }

}
