package com.highlycaffeinatedcode.domaintest.data.adapters;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Jon Cullen on 13/01/2018.
 */

public class DateTypeAdapter extends TypeAdapter<Date> {


    final private DateFormat ISO8601_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss");

    @Override
    public void write(JsonWriter out, Date value) throws IOException {
        if (value == null)
            out.nullValue();
        else
            out.value(value.getTime() / 1000);
    }

    @Override
    public Date read(JsonReader in) throws IOException {

        String dateString;
        JsonToken type = in.peek();
        switch (type) {
            case STRING:
                dateString = in.nextString();
                break;
            case NULL:
                in.nextNull();
                return null;
            default:
                throw new IllegalStateException("Expected string or null but was " + type);
        }

        //Cause some genius at .Net had never heard of ISO8601.
        // "/Date(00000000+0000)/"
        if (dateString.matches("/Date\\(\\d+\\+\\d+\\)/")) {
            Pattern p = Pattern.compile("-?\\d+"); // get time, ignoring timezone as not necessary;
            Matcher m = p.matcher(dateString);
            if (m.find()) {
                try {
                    return new Date(Long.parseLong(m.group()));
                } catch (NumberFormatException e) {
                    //continue;
                }
            }
        }

        try {
            ISO8601_FORMAT.parse(dateString);
        } catch (ParseException e) {
            //continue;
        }

        return null;

    }
}
