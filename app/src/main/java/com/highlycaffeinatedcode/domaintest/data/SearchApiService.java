package com.highlycaffeinatedcode.domaintest.data;

import com.highlycaffeinatedcode.domaintest.data.models.SearchResult;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Jon Cullen on 07/01/2018.
 */

public interface SearchApiService {

    enum QueryParamDefault {

        MODE("buy"),
        SUBURB("Bondi"),
        POSTCODE("2026"),
        STATE("NSW");

        private final String text;
        QueryParamDefault(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }


    @GET("mapsearch")
    Observable<SearchResult> getMapSearchResults(
            @Query("mode") String searchMode,
            @Query("sub") String suburb,
            @Query("pcodes") String postCodes,
            @Query("state") String state
    );
}
