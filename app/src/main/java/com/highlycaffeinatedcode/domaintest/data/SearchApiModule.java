package com.highlycaffeinatedcode.domaintest.data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.highlycaffeinatedcode.domaintest.data.adapters.BooleanTypeAdapter;
import com.highlycaffeinatedcode.domaintest.data.adapters.DateTypeAdapter;

import java.io.IOException;
import java.util.Date;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Jon Cullen on 07/01/2018.
 */

@Module
public class SearchApiModule {

    private final String SEARCH_BASE_URL = "https://rest.domain.com.au/searchservice.svc/";

    @Provides
    @Singleton
    OkHttpClient provideClient() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);

        return new OkHttpClient.Builder().addInterceptor(interceptor).addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request();
                HttpUrl url = request.url().newBuilder()
                        .build();
                request = request.newBuilder().url(url).build();
                return chain.proceed(request);
            }
        }).build();
    }

    @Provides
    @Singleton
    Gson provideGson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapter(Date.class,new DateTypeAdapter());
        gsonBuilder.registerTypeAdapter(boolean.class,new BooleanTypeAdapter());
        return gsonBuilder.create();
    }

    @Provides
    @Singleton
    Retrofit provideRetrofit(String baseURL, OkHttpClient client, Gson gson) {
        return new Retrofit.Builder()
                .baseUrl(baseURL)
                .client(client)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    @Provides
    @Singleton
    SearchApiService provideApiService() {
        return provideRetrofit(SEARCH_BASE_URL, provideClient(), provideGson()).create(SearchApiService.class);
    }

}
