package com.highlycaffeinatedcode.domaintest.data.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Jon Cullen on 07/01/2018.
 */

public class Listing {


    @SerializedName("AdId")
    @Expose
    private long adId;

    @SerializedName("AgencyColour")
    @Expose
    private String agencyColour;

    @SerializedName("AgencyLogoUrl")
    @Expose
    private String agencyLogoUrl;

    @SerializedName("Area")
    @Expose
    private String area;

    @SerializedName("Bathrooms")
    @Expose
    private int bathrooms;

    @SerializedName("Bedrooms")
    @Expose
    private int bedrooms;

    @SerializedName("Carspaces")
    @Expose
    private int carSpaces;

    @SerializedName("DisplayPrice")
    @Expose
    private String displayPrice;

    @SerializedName("DisplayableAddress")
    @Expose
    private String displayableAddress;

    @SerializedName("Headline")
    @Expose
    private String headline;

    @SerializedName("TruncatedDescription")
    @Expose
    private String truncatedDescription;

    @SerializedName("Description")
    @Expose
    private String description;

    @SerializedName("RetinaDisplayThumbUrl")
    @Expose
    private String retinaDisplayThumbUrl;

    @SerializedName("SecondRetinaDisplayThumbUrl")
    @Expose
    private String secondRetinaDisplayThumbUrl;

    @SerializedName("IsElite")
    @Expose
    private boolean elite;

    @SerializedName("DateFirstListed")
    @Expose
    private Date dateFirstListed;

    @SerializedName("DateUpdated")
    @Expose
    private Date dateUpdated;

    public long getAdId() {
        return adId;
    }

    public void setAdId(long adId) {
        this.adId = adId;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getAgencyLogoUrl() {
        return agencyLogoUrl;
    }

    public void setAgencyLogoUrl(String agencyLogoUrl) {
        this.agencyLogoUrl = agencyLogoUrl;
    }

    public int getBathrooms() {
        return bathrooms;
    }

    public void setBathrooms(int bathrooms) {
        this.bathrooms = bathrooms;
    }

    public int getBedrooms() {
        return bedrooms;
    }

    public void setBedrooms(int bedrooms) {
        this.bedrooms = bedrooms;
    }

    public int getCarSpaces() {
        return carSpaces;
    }

    public void setCarSpaces(int carSpaces) {
        this.carSpaces = carSpaces;
    }

    public String getDisplayPrice() {
        return displayPrice;
    }

    public void setDisplayPrice(String displayPrice) {
        this.displayPrice = displayPrice;
    }

    public String getDisplayableAddress() {
        return displayableAddress;
    }

    public void setDisplayableAddress(String displayableAddress) {
        this.displayableAddress = displayableAddress;
    }

    public String getTruncatedDescription() {
        return truncatedDescription;
    }

    public void setTruncatedDescription(String truncatedDescription) {
        this.truncatedDescription = truncatedDescription;
    }

    public String getRetinaDisplayThumbUrl() {
        return retinaDisplayThumbUrl;
    }

    public void setRetinaDisplayThumbUrl(String retinaDisplayThumbUrl) {
        this.retinaDisplayThumbUrl = retinaDisplayThumbUrl;
    }

    public String getSecondRetinaDisplayThumbUrl() {
        return secondRetinaDisplayThumbUrl;
    }

    public void setSecondRetinaDisplayThumbUrl(String secondRetinaDisplayThumbUrl) {
        this.secondRetinaDisplayThumbUrl = secondRetinaDisplayThumbUrl;
    }

    public boolean isElite() {
        return elite;
    }

    public void setElite(boolean elite) {
        this.elite = elite;
    }


    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAgencyColour() {
        return agencyColour;
    }

    public void setAgencyColour(String agencyColour) {
        this.agencyColour = agencyColour;
    }

    public Date getDateFirstListed() {
        return dateFirstListed;
    }

    public void setDateFirstListed(Date dateFirstListed) {
        this.dateFirstListed = dateFirstListed;
    }

    public Date getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Date dateUpdated) {
        this.dateUpdated = dateUpdated;
    }
}
