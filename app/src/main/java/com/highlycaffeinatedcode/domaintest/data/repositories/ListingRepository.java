package com.highlycaffeinatedcode.domaintest.data.repositories;

import com.highlycaffeinatedcode.domaintest.data.models.Listing;

import rx.Observable;

/**
 * Created by Jon Cullen on 07/01/2018.
 */

public interface ListingRepository {

    //TODO: Implement Search Params
    Observable<Listing> getPropertyListings(boolean refresh);
    Observable<Listing> getPropertyListingsFromMemory();
    Observable<Listing> getPropertyListingsFromNetwork();

    boolean isFavourite(long adId);
    void addFavourite(long adId);
    void removeFavourite(long adId);

}
