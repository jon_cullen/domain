package com.highlycaffeinatedcode.domaintest.app;

import com.highlycaffeinatedcode.domaintest.data.SearchApiModule;
import com.highlycaffeinatedcode.domaintest.ui.activities.propertylist.PropertyListActivity;
import com.highlycaffeinatedcode.domaintest.ui.activities.propertylist.PropertyListModule;
import com.highlycaffeinatedcode.domaintest.ui.adapters.PropertyListAdapter;
import com.highlycaffeinatedcode.domaintest.ui.fragments.PropertyDetailFragment;
import com.squareup.otto.Bus;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Jon Cullen on 07/01/2018.
 */

@Singleton
@Component(modules = {ApplicationModule.class, PropertyListModule.class, SearchApiModule.class})
public interface ApplicationComponent {

    void inject(PropertyListActivity target);
    void inject(PropertyDetailFragment fragment);
    void inject(PropertyListAdapter adapter);

    Bus provideBus();

}
