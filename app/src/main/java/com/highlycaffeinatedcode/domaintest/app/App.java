package com.highlycaffeinatedcode.domaintest.app;

import android.app.Application;
import android.content.Context;

/**
 * Created by Jon Cullen on 07/01/2018.
 */

public class App extends Application {

    private ApplicationComponent component;

    private static App staticApp;
    private static Context staticContext;

    public static Context getContext() {
        return staticContext;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        // create static instances
        App.staticContext = getApplicationContext();
        App.staticApp = this;

        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    public static synchronized App getInstance() {
        return staticApp;
    }

    public ApplicationComponent getComponent() {
        return component;
    }



}
