package com.highlycaffeinatedcode.domaintest.events;

/**
 * Created by Jon Cullen on 14/01/2018.
 */

public class FavouriteClickEvent {

    private long adId;
    private boolean favourite;
    private boolean notify;

    public FavouriteClickEvent(long adId, boolean favourite) {
        this(adId,favourite,false);
    }
    public FavouriteClickEvent(long adId, boolean favourite, boolean notify) {
        this.adId = adId;
        this.favourite = favourite;
        this.notify = notify;
    }

    public long getAdId() {
        return adId;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public boolean isNotify() {
        return notify;
    }
}
