package com.highlycaffeinatedcode.domaintest.ui.adapters;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.highlycaffeinatedcode.domaintest.R;
import com.highlycaffeinatedcode.domaintest.app.App;
import com.highlycaffeinatedcode.domaintest.events.FavouriteClickEvent;
import com.highlycaffeinatedcode.domaintest.ui.activities.propertydetail.PropertyDetailActivity;
import com.highlycaffeinatedcode.domaintest.ui.fragments.PropertyDetailFragment;
import com.highlycaffeinatedcode.domaintest.ui.models.PropertyViewModel;
import com.highlycaffeinatedcode.domaintest.ui.activities.propertylist.PropertyListActivity;
import com.highlycaffeinatedcode.domaintest.utils.Utils;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jon Cullen on 07/01/2018.
 */

public class PropertyListAdapter extends RecyclerView.Adapter<PropertyListAdapter.PropertyViewHolder> {

    @Inject
    Bus bus;

    private PropertyListActivity parentActivity;
    private boolean splitScreen;
    private List<PropertyViewModel> propertyList;
    Animation bounceAnim;

    public PropertyListAdapter(PropertyListActivity parentActivity, boolean splitScreen, List<PropertyViewModel> propertyList) {
        this.parentActivity = parentActivity;
        this.splitScreen = splitScreen;
        this.propertyList = propertyList;
        this.bounceAnim = AnimationUtils.loadAnimation(parentActivity, R.anim.bounce);

        App.getInstance().getComponent().inject(this);

    }

    @Override
    public PropertyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        switch (viewType) {
            case PropertyListingType.PREMIUM:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_property_premium, parent, false);
                return new PremiumListingViewHolder(itemView);
            default:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_property, parent, false);
                return new StandardListingViewHolder(itemView);
        }

    }

    @Override
    public void onBindViewHolder(PropertyViewHolder holder, int position) {
        PropertyViewModel property = propertyList.get(position);
        PropertyListingType propertyListItem = new PropertyListItem(property);
        holder.bindType(propertyListItem);
    }

    @Override
    public int getItemCount() {
        return propertyList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return new PropertyListItem(propertyList.get(position)).getListItemType();
    }

    public void setFavourite(long adId, boolean isFavourite, boolean notify) {

        for (PropertyViewModel property : propertyList) {
            if (property.getAdId() == adId) {
                property.setFavourite(isFavourite);
                if (notify) notifyDataSetChanged(); //Was initially calling notifyItemChanged, but it messes with animations;
                break;
            }
        }
    }

    public interface PropertyListingType {
        int STANDARD = 0;
        int PREMIUM = 1;
    }

    public class PropertyListItem implements PropertyListingType {

        private PropertyViewModel property;

        private PropertyListItem(PropertyViewModel property) {
            this.property = property;
        }

        private PropertyViewModel getProperty() {
            return property;
        }

        public int getListItemType() {
            return property.isElite() ? PropertyListingType.PREMIUM : PropertyListingType.STANDARD;
        }
    }

    public abstract class PropertyViewHolder extends RecyclerView.ViewHolder {

        private PropertyViewHolder(View itemView) {
            super(itemView);
        }

        public abstract void bindType(PropertyListingType propertyListing);

    }

    public class StandardListingViewHolder extends PropertyViewHolder {

        protected PropertyViewModel property;

        @BindView(R.id.property_info_background)
        RelativeLayout propertyInfoBackground;
        @BindView(R.id.property_price)
        TextView propertyPrice;
        @BindView(R.id.property_type)
        TextView propertyType;
        @BindView(R.id.property_address)
        TextView propertyAddress;
        @BindView(R.id.property_image_1)
        ImageView propertyImage1;
        @BindView(R.id.property_agent_image)
        ImageView propertyAgentImage;
        @BindView(R.id.property_agent_layout)
        LinearLayout propertyAgentLayout;
        @BindView(R.id.property_new_badge)
        RelativeLayout propertyNewBadge;
        @BindView(R.id.property_favourite)
        ImageView propertyFavourite;

        private StandardListingViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

        }

        public void bindType(PropertyListingType propertyListing) {

            property = ((PropertyListItem) propertyListing).getProperty();
            propertyPrice.setText(property.getPrice());
            propertyType.setText(property.getType());
            propertyAddress.setText(property.getAddress());
            propertyAgentLayout.setBackgroundColor(property.getAgencyColor());

            Utils.assignImageUrl(property.getImage1Url(), propertyImage1);
            Utils.assignImageUrl(property.getAgencyLogoUrl(), propertyAgentImage);

            propertyNewBadge.setVisibility(property.isNewListing() ? View.VISIBLE : View.GONE);
            propertyFavourite.setBackgroundResource(property.isFavourite() ? R.drawable.favourite_on : R.drawable.favourite_off);


            itemView.setTag(property);
            itemView.setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            PropertyViewModel property = (PropertyViewModel) view.getTag();
                            String propertyJson = new Gson().toJson(property);

                            if (splitScreen) {

                                Bundle arguments = new Bundle();
                                arguments.putBoolean(PropertyDetailFragment.ARG_SPLITSCREEN, true);
                                arguments.putString(PropertyDetailFragment.ARG_PROPERTY, propertyJson);
                                PropertyDetailFragment fragment = new PropertyDetailFragment();
                                fragment.setArguments(arguments);
                                parentActivity.getSupportFragmentManager()
                                        .beginTransaction()
                                        .replace(R.id.detail_container, fragment)
                                        .commit();

                            } else {

                                Intent intent = new Intent(parentActivity, PropertyDetailActivity.class);
                                intent.putExtra(PropertyDetailFragment.ARG_PROPERTY, propertyJson);

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                                    List<Pair<View, String>> pairs = new ArrayList<>();
                                    pairs.add(Pair.create((View) propertyImage1, propertyImage1.getTransitionName()));
                                    pairs.add(Pair.create((View) propertyInfoBackground, propertyInfoBackground.getTransitionName()));
                                    if (propertyNewBadge.getVisibility() == View.VISIBLE) {
                                        pairs.add(Pair.create((View) propertyNewBadge, propertyNewBadge.getTransitionName()));
                                    }

                                    ActivityOptionsCompat options =
                                            ActivityOptionsCompat.makeSceneTransitionAnimation(
                                                    parentActivity, pairs.toArray(new Pair[pairs.size()]));


                                    ActivityCompat.startActivity(parentActivity, intent, options.toBundle());

                                } else {
                                    parentActivity.startActivity(intent);
                                }
                            }
                        }
                    }

            );
        }

        @OnClick(R.id.property_favourite)
        void onFavouriteClick() {
            property.setFavourite(!property.isFavourite());
            propertyFavourite.setBackgroundResource(property.isFavourite() ? R.drawable.favourite_on : R.drawable.favourite_off);
            propertyFavourite.startAnimation(bounceAnim);
            bus.post(new FavouriteClickEvent(property.getAdId(), property.isFavourite()));

        }


    }

    public class PremiumListingViewHolder extends StandardListingViewHolder {

        @BindView(R.id.property_image_2)
        ImageView propertyImage2;

        private PremiumListingViewHolder(View itemView) {
            super(itemView);
        }

        @Override
        public void bindType(PropertyListingType propertyListing) {
            super.bindType(propertyListing);
            Utils.assignImageUrl(property.getImage2Url(), propertyImage2);
        }
    }


}
