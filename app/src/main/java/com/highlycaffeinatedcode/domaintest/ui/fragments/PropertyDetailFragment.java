package com.highlycaffeinatedcode.domaintest.ui.fragments;

import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.highlycaffeinatedcode.domaintest.R;
import com.highlycaffeinatedcode.domaintest.app.App;
import com.highlycaffeinatedcode.domaintest.events.FavouriteClickEvent;
import com.highlycaffeinatedcode.domaintest.ui.models.PropertyViewModel;
import com.highlycaffeinatedcode.domaintest.utils.Utils;
import com.squareup.otto.Bus;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jon Cullen on 07/01/2018.
 */

public class PropertyDetailFragment extends Fragment {

    public static final String ARG_PROPERTY = "property";
    public static final String ARG_SPLITSCREEN = "splitScreen";

    PropertyViewModel property;
    FragmentActivity parentActivity;
    boolean splitScreen;
    CollapsingToolbarLayout appBarLayout;
    Animation bounceAnim;

    @Inject
    Bus bus;

    @BindView(R.id.property_info_background)
    RelativeLayout propertyInfoBackground;
    @BindView(R.id.property_price)
    TextView propertyPrice;
    @BindView(R.id.property_type)
    TextView propertyType;
    @BindView(R.id.property_address)
    TextView propertyAddress;
    @BindView(R.id.property_agent_image)
    ImageView propertyAgentImage;
    @BindView(R.id.property_headline)
    TextView propertyHeadline;
    @BindView(R.id.property_description)
    TextView propertyDescription;
    @BindView(R.id.property_listing_id)
    TextView propertyListingId;
    @BindView(R.id.property_image_2)
    ImageView propertyImage2;
    @BindView(R.id.property_agent_layout)
    LinearLayout propertyAgentLayout;
    @BindView(R.id.property_favourite)
    ImageView propertyFavourite;

    public PropertyDetailFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        parentActivity = this.getActivity();
        App.getInstance().getComponent().inject(this);


        Bundle arguments = getArguments();
        if (arguments.containsKey(ARG_PROPERTY)) {
            property = new Gson().fromJson(arguments.getString(ARG_PROPERTY), PropertyViewModel.class);
        }
        splitScreen = arguments.getBoolean(ARG_SPLITSCREEN, false);

        bounceAnim = AnimationUtils.loadAnimation(parentActivity, R.anim.bounce);

        appBarLayout = parentActivity.findViewById(R.id.toolbar_layout);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            setAppBarTitle();

        } else {

            // Don't set the title until the animation completes - looks nasty otherwise
            Transition sharedElementEnterTransition = parentActivity.getWindow().getSharedElementEnterTransition();
            if (sharedElementEnterTransition != null) {

                sharedElementEnterTransition.addListener(new Transition.TransitionListener() {
                    @Override
                    public void onTransitionStart(@NonNull Transition transition) {

                    }

                    @Override
                    public void onTransitionEnd(@NonNull Transition transition) {
                        setAppBarTitle();
                    }

                    @Override
                    public void onTransitionCancel(@NonNull Transition transition) {

                    }

                    @Override
                    public void onTransitionPause(@NonNull Transition transition) {

                    }

                    @Override
                    public void onTransitionResume(@NonNull Transition transition) {

                    }
                });
            }
        }

    }

    @OnClick(R.id.property_favourite)
    void onFavouriteClick() {
        property.setFavourite(!property.isFavourite());
        bus.post(new FavouriteClickEvent(property.getAdId(),property.isFavourite(),true));
        propertyFavourite.setBackgroundResource(property.isFavourite() ? R.drawable.favourite_on : R.drawable.favourite_off);
        propertyFavourite.startAnimation(bounceAnim);
    }

    private void setAppBarTitle() {
        if (appBarLayout != null && property != null) {
            appBarLayout.setTitle(property.getArea());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_property_detail, container, false);

        ButterKnife.bind(this, rootView);

        if (property != null) {
            propertyPrice.setText(property.getPrice());
            propertyType.setText(property.getType());
            propertyAddress.setText(property.getAddress());
            propertyHeadline.setText(property.getHeadline());
            propertyDescription.setText(property.getDescription());
            propertyListingId.setText(Long.toString(property.getAdId()));
            propertyAgentLayout.setBackgroundColor(property.getAgencyColor());

            Utils.assignImageUrl(property.getAgencyLogoUrl(),propertyAgentImage);
            Utils.assignImageUrl(property.getImage2Url(), propertyImage2);
            propertyFavourite.setBackgroundResource(property.isFavourite() ? R.drawable.favourite_on : R.drawable.favourite_off);


        }
        if (splitScreen) {
            propertyInfoBackground.setVisibility(View.GONE);
        }

        return rootView;
    }



}
