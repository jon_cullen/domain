package com.highlycaffeinatedcode.domaintest.ui.models;

import com.highlycaffeinatedcode.domaintest.data.models.Listing;
import com.highlycaffeinatedcode.domaintest.ui.activities.propertylist.PropertyListMVP;
import com.highlycaffeinatedcode.domaintest.data.repositories.ListingRepository;

import retrofit2.adapter.rxjava.Result;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Jon Cullen on 07/01/2018.
 */

public class PropertyListModel implements PropertyListMVP.Model {

    private ListingRepository repository;

    public PropertyListModel(ListingRepository repository) {
        this.repository = repository;
    }

    @Override
    public Observable<PropertyViewModel> getPropertyListings(boolean refresh) {

        return repository.getPropertyListings(refresh).flatMap(
                new Func1<Listing, Observable<PropertyViewModel>>() {
                    @Override
                    public Observable<PropertyViewModel> call(Listing listing) {
                        boolean isFavourite = repository.isFavourite(listing.getAdId());
                        return Observable.just(new PropertyViewModel(listing, isFavourite ));
                    }
                }
        );
    }

    @Override
    public void addFavourite(long adId) {
        repository.addFavourite(adId);
    }

    @Override
    public void removeFavourite(long adId) {
        repository.removeFavourite(adId);
    }

}
