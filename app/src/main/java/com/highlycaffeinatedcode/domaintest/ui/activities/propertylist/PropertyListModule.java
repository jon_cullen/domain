package com.highlycaffeinatedcode.domaintest.ui.activities.propertylist;

import com.highlycaffeinatedcode.domaintest.data.SearchApiService;
import com.highlycaffeinatedcode.domaintest.ui.models.PropertyListModel;
import com.highlycaffeinatedcode.domaintest.data.repositories.ListingRepository;
import com.highlycaffeinatedcode.domaintest.data.repositories.ListingRepositoryImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Jon Cullen on 07/01/2018.
 */

@Module
public class PropertyListModule {

    @Provides
    public PropertyListMVP.Presenter provideMainActivityPresenter(PropertyListMVP.Model propertyListModel) {
        return new PropertyListPresenter(propertyListModel);
    }

    @Provides
    public PropertyListMVP.Model provideMainActivityModel(ListingRepository repository) {
        return new PropertyListModel(repository);
    }

    @Singleton
    @Provides
    public ListingRepository provideRepository(SearchApiService searchApiService) {
        return new ListingRepositoryImpl(searchApiService);
    }
}
