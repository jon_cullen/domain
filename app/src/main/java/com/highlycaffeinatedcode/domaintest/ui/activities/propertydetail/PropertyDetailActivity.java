package com.highlycaffeinatedcode.domaintest.ui.activities.propertydetail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.google.gson.Gson;
import com.highlycaffeinatedcode.domaintest.R;
import com.highlycaffeinatedcode.domaintest.ui.fragments.PropertyDetailFragment;
import com.highlycaffeinatedcode.domaintest.ui.models.PropertyViewModel;
import com.highlycaffeinatedcode.domaintest.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jon Cullen on 07/01/2018.
 */

public class PropertyDetailActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.property_image_1)
    ImageView propertyImage1;
    @BindView(R.id.property_new_badge)
    RelativeLayout propertyNewBadge;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        overridePendingTransition(R.anim.fade_in, R.anim.fade_none);

        setContentView(R.layout.activity_property_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        String propertyJson = getIntent().getStringExtra(PropertyDetailFragment.ARG_PROPERTY);
        if (propertyJson != null) {
            PropertyViewModel property = new Gson().fromJson(propertyJson, PropertyViewModel.class);
            Utils.assignImageUrl(property.getImage1Url(), propertyImage1);
            propertyNewBadge.setVisibility(property.isNewListing() ? View.VISIBLE : View.GONE);
        }

        if (savedInstanceState == null) {
            Bundle arguments = new Bundle();
            arguments.putString(PropertyDetailFragment.ARG_PROPERTY,propertyJson);
            PropertyDetailFragment fragment = new PropertyDetailFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.detail_container, fragment)
                    .commit();
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case  android.R.id.home:
                this.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }




}
