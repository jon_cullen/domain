package com.highlycaffeinatedcode.domaintest.ui.models;

import android.graphics.Color;
import android.text.TextUtils;

import com.highlycaffeinatedcode.domaintest.R;
import com.highlycaffeinatedcode.domaintest.app.App;
import com.highlycaffeinatedcode.domaintest.data.models.Listing;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Jon Cullen on 07/01/2018.
 */

public class PropertyViewModel {

    private long adId;
    private String agencyLogoUrl;
    private String agencyColour;
    private String area;
    private int bathrooms;
    private int bedrooms;
    private int carSpaces;
    private String price;
    private String address;
    private String headline;
    private String description;
    private String image1Url;
    private String image2Url;
    private boolean elite;
    private boolean newListing;
    private boolean favourite;

    private final static int DAYS_FOR_NEW = 7;

    public PropertyViewModel(Listing listing, boolean isFavourite) {

        this.adId = listing.getAdId();
        this.agencyLogoUrl = listing.getAgencyLogoUrl();
        this.agencyColour = listing.getAgencyColour();
        this.area = listing.getArea();
        this.bathrooms = listing.getBathrooms();
        this.bedrooms = listing.getBedrooms();
        this.carSpaces = listing.getCarSpaces();
        this.headline = listing.getHeadline();
        this.price = listing.getDisplayPrice();
        this.address = listing.getDisplayableAddress();
        this.description = listing.getDescription();
        this.image1Url = listing.getRetinaDisplayThumbUrl();
        this.image2Url = listing.getSecondRetinaDisplayThumbUrl();
        this.elite = listing.isElite();

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR,-DAYS_FOR_NEW);
        this.newListing = listing.getDateFirstListed().after(calendar.getTime());

        this.favourite = isFavourite;
    }


    public String getAgencyLogoUrl() {
        return agencyLogoUrl;
    }

    public String getArea() {
        return area;
    }

    public int getBathrooms() {
        return bathrooms;
    }

    public int getBedrooms() {
        return bedrooms;
    }

    public int getCarSpaces() {
        return carSpaces;
    }

    public String getPrice() {
        return price;
    }

    public String getAddress() {
        return address;
    }

    public String getDescription() {
        return description;
    }

    public String getHeadline() {
        return headline;
    }

    public String getImage1Url() {
        return image1Url;
    }

    public String getImage2Url() {
        return image2Url;
    }

    public boolean isElite() {
        return elite;
    }

    public long getAdId() {
        return adId;
    }

    public boolean isNewListing() {
        return newListing;
    }

    public String getAgencyColour() {
        return agencyColour;
    }

    public void setAgencyColour(String agencyColour) {
        this.agencyColour = agencyColour;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public String getType() {
        ArrayList<String> props = new ArrayList<>();
        if (this.bedrooms > 0) {
            props.add(bedrooms + " " + App.getContext().getString(R.string.property_description_bed));
        }
        if (this.bathrooms > 0) {
            props.add(bathrooms + " " + App.getContext().getString(R.string.property_description_bath));
        }
        if (this.carSpaces > 0) {
            props.add(carSpaces + " "  + App.getContext().getString(R.string.property_description_car));
        }
        return TextUtils.join(", ", props);
    }

    public int getAgencyColor() {
        try {
            return Color.parseColor(agencyColour);
        } catch (IllegalStateException | NullPointerException e) {
            return 0;
        }
    }
}
