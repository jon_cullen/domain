package com.highlycaffeinatedcode.domaintest.ui.activities.propertylist;

import com.highlycaffeinatedcode.domaintest.ui.models.PropertyViewModel;

import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jon Cullen on 07/01/2018.
 */

public class PropertyListPresenter implements PropertyListMVP.Presenter {


    private PropertyListMVP.View view;
    private Subscription subscription = null;
    private PropertyListMVP.Model model;

    public PropertyListPresenter(PropertyListMVP.Model model) {
        this.model = model;
    }

    @Override
    public void loadData(boolean refresh) {

        subscription = model
                .getPropertyListings(refresh)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<PropertyViewModel>() {
                    @Override
                    public void onCompleted() {
                        if (view != null) {
                            view.onUpdateComplete();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        if (view != null) {
                            view.onUpdateComplete();
                            view.showSnackbar("Error Retrieving Data");
                        }
                    }

                    @Override
                    public void onNext(PropertyViewModel viewModel) {
                        if (view != null) {
                            view.updateData(viewModel);
                        }
                    }
                });

    }

    @Override
    public void unsubscribe() {
        if (subscription != null && !subscription.isUnsubscribed()) {
            subscription.unsubscribe();
        }
    }

    @Override
    public void setView(PropertyListMVP.View view) {
        this.view = view;
    }

    @Override
    public void setFavourite(long adId, boolean isFavourite) {
        if (isFavourite)
            model.addFavourite(adId);
        else
            model.removeFavourite(adId);
    }

}


