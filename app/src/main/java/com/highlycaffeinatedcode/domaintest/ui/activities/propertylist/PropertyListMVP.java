package com.highlycaffeinatedcode.domaintest.ui.activities.propertylist;

import com.highlycaffeinatedcode.domaintest.ui.models.PropertyViewModel;
import rx.Observable;

/**
 * Created by Jon Cullen on 07/01/2018.
 */

public class PropertyListMVP {


    public interface View {

        void updateData(PropertyViewModel viewModel);
        void onUpdateComplete();
        void showSnackbar(String s);

    }

    public interface Presenter {

        void loadData(boolean refresh);
        void unsubscribe();
        void setView(PropertyListMVP.View view);
        void setFavourite(long adId, boolean isFavourite);

    }

    public interface Model {

        Observable<PropertyViewModel> getPropertyListings(boolean refresh);
        void addFavourite(long adId);
        void removeFavourite(long adID);

    }



}
