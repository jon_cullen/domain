package com.highlycaffeinatedcode.domaintest.ui.activities.propertylist;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.highlycaffeinatedcode.domaintest.R;
import com.highlycaffeinatedcode.domaintest.events.FavouriteClickEvent;
import com.highlycaffeinatedcode.domaintest.ui.adapters.PropertyListAdapter;
import com.highlycaffeinatedcode.domaintest.app.App;
import com.highlycaffeinatedcode.domaintest.ui.models.PropertyViewModel;
import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jon Cullen on 07/01/2018.
 */

public class PropertyListActivity extends AppCompatActivity implements PropertyListMVP.View {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout refreshLayout;

    @Inject
    PropertyListMVP.Presenter presenter;
    @Inject
    Bus bus;

    private PropertyListAdapter listAdapter;
    private List<PropertyViewModel> propertyList = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_property_list);

        ((App) getApplication()).getComponent().inject(this);
        ButterKnife.bind(this);
        bus.register(this);

        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());

        listAdapter = new PropertyListAdapter(this, findViewById(R.id.detail_container) != null, propertyList);
        recyclerView.setAdapter(listAdapter);

        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                propertyList.clear();
                listAdapter.notifyDataSetChanged();
                presenter.loadData(true);
            }
        });

        refreshLayout.setRefreshing(true);
        presenter.loadData(false);


    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.setView(this);
        overridePendingTransition(R.anim.fade_none, R.anim.fade_out);
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.unsubscribe();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        bus.unregister(this);
    }

    @Override
    public void updateData(PropertyViewModel viewModel) {
        propertyList.add(viewModel);
        listAdapter.notifyItemInserted(propertyList.size() - 1);
    }

    @Override
    public void onUpdateComplete() {
        refreshLayout.setRefreshing(false);
    }

    @Override
    public void showSnackbar(String s) {
        Snackbar.make(refreshLayout, s, Snackbar.LENGTH_LONG).show();
    }

    @Subscribe
    public void onFavouriteClickEvent(FavouriteClickEvent event) {
        presenter.setFavourite(event.getAdId(), event.isFavourite());
        listAdapter.setFavourite(event.getAdId(),event.isFavourite(),event.isNotify());
    }


}
