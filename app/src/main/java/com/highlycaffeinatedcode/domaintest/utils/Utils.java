package com.highlycaffeinatedcode.domaintest.utils;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;

import com.highlycaffeinatedcode.domaintest.R;
import com.highlycaffeinatedcode.domaintest.app.App;
import com.squareup.picasso.Picasso;

/**
 * Created by Jon Cullen on 08/01/2018.
 */

public class Utils {

    public static void assignImageUrl(String url, ImageView imageView) {

        if (TextUtils.isEmpty(url)) {
            imageView.setVisibility(View.GONE);
        } else {
            Picasso.with(App.getContext()).load(url)
                    .placeholder(R.color.white) // don't show placeholder while loading
                    .error(R.drawable.property_placeholder) // return placeholder on error
                    .into(imageView);
        }
    }
}
